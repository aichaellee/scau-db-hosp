package scau.hosp.dictionary;

import scau.hosp.base.common.IBaseCode;

public enum GlobalCode implements IBaseCode {
    OPERATION_SUCCESS(1, "操作成功"),
    OPERATION_ADDFAIL(2, "添加失败"),
    OPEARTION_FAIL(3, "操作失败")

    ;
    private int code;
    private String message;
    GlobalCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
    public int getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
