package scau.hosp.web;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import scau.hosp.base.common.BaseController;
import scau.hosp.dictionary.GlobalCode;
import scau.hosp.entity.Department;
import scau.hosp.service.IDepartmentService;

@Api(description = "科室控制器")
@RestController
@RequestMapping("/department")
public class DepartmentController extends BaseController {
	
	@Autowired
	IDepartmentService iDepartmentService;
	
	@ApiOperation("新添")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "deparmentNumber", dataType = "String", required = true, value = "科室号"),
		@ApiImplicitParam(paramType = "query", name = "name", dataType = "String", required = true, value = "科室名字"),
		@ApiImplicitParam(paramType = "query", name = "address", dataType = "String", required = true, value = "科室地址"),
		@ApiImplicitParam(paramType = "query", name = "phone", dataType = "String", value = "科室电话")
	})
	@PostMapping("/add")
	public Map<String, Object> addDepartment(Department department, @RequestParam String deparmentNumber,
			@RequestParam String name, @RequestParam String address) {
		Map<String, Object> model = new LinkedHashMap<>();
		department.setStatus(1);
		if (iDepartmentService.add(department) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPERATION_ADDFAIL);
		return model;
	}
	
	@ApiOperation("删除(作废)")
	@PostMapping("/delete")
	public Map<String, Object> deleteDepartment(Department department, @RequestParam List<Integer> idList) {
		Map<String, Object> model = new LinkedHashMap<>();
		for (Integer deleteId: idList) {
			department.setId(deleteId);
			department.setStatus(0);
			iDepartmentService.updateNotNull(department);
		}
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("全部有效科室")
	@ApiResponses(@ApiResponse(code = 200,message = "详情",response = Department.class))
	@GetMapping("/show")
	public Map<String, Object> showBinglidan(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer pageSize){
		Map<String, Object> model = new LinkedHashMap<>();
		PageHelper.startPage(page, pageSize);
		List<Department> departments = iDepartmentService.findSelectPropertyWhereAnd("status", 1);
		PageInfo<Department> pageInfo = new PageInfo<Department>(departments);
		model.put("content", departments);
		model.put("total", pageInfo.getTotal());
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("修改")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "id", dataType = "Integer", required = true, value = "id"),
		@ApiImplicitParam(paramType = "query", name = "deparmentNumber", dataType = "String", value = "科室号"),
		@ApiImplicitParam(paramType = "query", name = "name", dataType = "String", value = "科室名字"),
		@ApiImplicitParam(paramType = "query", name = "address", dataType = "String", value = "科室地址"),
		@ApiImplicitParam(paramType = "query", name = "phone", dataType = "String", value = "科室电话")
	})
	@PostMapping("/update")
	public Map<String, Object> updateDepartment(Department department, @RequestParam Integer id) {
		Map<String, Object> model = new LinkedHashMap<>();
		if (iDepartmentService.updateNotNull(department) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPEARTION_FAIL);
		return model;
	}
}

