package scau.hosp.web;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import scau.hosp.base.common.BaseController;
import scau.hosp.dictionary.GlobalCode;
import scau.hosp.entity.Jianyandan;
import scau.hosp.entity.JianyandanAll;
import scau.hosp.service.IJianyandanService;

@Api(description = "检验单控制器")
@RestController
@RequestMapping("/jianyandan")
public class JianyandanController extends BaseController {
 
	@Autowired
	IJianyandanService iJianyandanService;
	
	@ApiOperation("生成")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "patientId", dataType = "String", required = true, value = "病人id"),
		@ApiImplicitParam(paramType = "query", name = "doctorId", dataType = "String", required = true, value = "医生id"),
		@ApiImplicitParam(paramType = "query", name = "project", dataType = "String", required = true, value = "出院日期"),
		@ApiImplicitParam(paramType = "query", name = "cost", dataType = "Double", required = true, value = "状态 0出院 1住院"),
	})
	@PostMapping("/add")
	public Map<String, Object> addJianyandan(Jianyandan jianyandan, @RequestParam String patientId,
			@RequestParam String doctorId, @RequestParam String project, @RequestParam Double cost) {
		Map<String, Object> model = new LinkedHashMap<>();
		jianyandan.setStatus(1);
		if (iJianyandanService.add(jianyandan) == 1) {
			sendCode(model, GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model, GlobalCode.OPERATION_ADDFAIL);
		return model;
	}
	
	@ApiOperation("删除(作废)")
	@PostMapping("/delete")
	public Map<String, Object> deleteJianyandan(Jianyandan jianyandan, @RequestParam List<Integer> idList) {
		Map<String, Object> model = new LinkedHashMap<>();
		for (Integer deleteId: idList) {
			jianyandan.setId(deleteId);
			jianyandan.setStatus(0);
			iJianyandanService.updateNotNull(jianyandan);
		}
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("检验单信息")
	@ApiResponses(@ApiResponse(code = 200,message = "详情",response = Jianyandan.class))
	@GetMapping("/show")
	public Map<String, Object> showJianyandan(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer pageSize){
		Map<String, Object> model = new LinkedHashMap<>();
		PageHelper.startPage(page, pageSize);
		List<JianyandanAll> jianyandanAlls = iJianyandanService.findAllJianyandan();
		PageInfo<JianyandanAll> pageInfo = new PageInfo<JianyandanAll>(jianyandanAlls);
		model.put("content", jianyandanAlls);
		model.put("total", pageInfo.getTotal());
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "id", dataType = "Integer", required = true, value = "检验单id"),
		@ApiImplicitParam(paramType = "query", name = "jianyanTime", dataType = "Date", value = "检验日期"),
		@ApiImplicitParam(paramType = "query", name = "result", dataType = "String", value = "检验结果"),
	})
	@PostMapping("/update")
	public Map<String, Object> updateJianyandan(Jianyandan jianyandan, @RequestParam Integer id) {
		Map<String, Object> model = new LinkedHashMap<>();
		if (iJianyandanService.updateNotNull(jianyandan) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPEARTION_FAIL);
		return model;
	}
}

