package scau.hosp.web;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import scau.hosp.base.common.BaseController;
import scau.hosp.dictionary.GlobalCode;
import scau.hosp.entity.Sickroom;
import scau.hosp.entity.SickroomAll;
import scau.hosp.service.ISickroomService;

@Api(description = "病房控制器")
@RestController
@RequestMapping("/sickroom")
public class SickroomController extends BaseController {
 
	@Autowired
	ISickroomService iSickroomService;
	
	@ApiOperation("添加")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "roomNumber", dataType = "String", required = true, value = "病房号"),
		@ApiImplicitParam(paramType = "query", name = "bedNumber", dataType = "String", required = true, value = "病床号"),
		@ApiImplicitParam(paramType = "query", name = "departmentId", dataType = "Integer", value = "科室id"),
	})
	@PostMapping("/add")
	public Map<String, Object> addSickroom(Sickroom sickroom, @RequestParam String roomNumber, 
			@RequestParam String bedNumber, @RequestParam String departmentId){
		Map<String, Object> model = new LinkedHashMap<>();
		sickroom.setBedStatus(1);
		sickroom.setStatus(1);
		if (iSickroomService.add(sickroom) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPERATION_ADDFAIL);
		return model;
	}
	
    @ApiOperation("删除(作废)")
    @PostMapping("/delete")
    public Map<String, Object> deleteSickroom(Sickroom sickroom , @RequestParam("id") List<Integer> idList) {
        Map<String, Object> model = new LinkedHashMap<>();
		for (Integer deleteId: idList) {
			sickroom.setId(deleteId);
			sickroom.setStatus(0);
			iSickroomService.updateNotNull(sickroom);
		}
        sendCode(model, GlobalCode.OPERATION_SUCCESS);
        return model;
    }

	@ApiOperation("病房信息")
	@ApiResponses(@ApiResponse(code = 200,message = "详情",response = Sickroom.class))
	@GetMapping("show")
	public Map<String, Object> showPatint(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer pageSize){
		Map<String, Object> model = new LinkedHashMap<>();
		PageHelper.startPage(page, pageSize);
		List<SickroomAll> sickroomAlls = iSickroomService.findAllSickroom();
		PageInfo<SickroomAll> pageInfo = new PageInfo<SickroomAll>(sickroomAlls);
		model.put("content", sickroomAlls);
		model.put("total", pageInfo.getTotal());
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("修改信息")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "id", dataType = "Integer", value = "id"),
		@ApiImplicitParam(paramType = "query", name = "roomNumber", dataType = "String", value = "病房号"),
		@ApiImplicitParam(paramType = "query", name = "bedNumber", dataType = "String", value = "病床号"),
		@ApiImplicitParam(paramType = "query", name = "departmentId", dataType = "Integer", value = "科室id"),
		@ApiImplicitParam(paramType = "query", name = "bedStatus", dataType = "Integer", value = "病床状态 0住人 1空闲"),
	})
	@PostMapping("/update")
	public Map<String, Object> updateSickroom(Sickroom sickroom, @RequestParam Integer id){
 		Map<String, Object> model = new LinkedHashMap<>();
		if (iSickroomService.updateNotNull(sickroom) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPEARTION_FAIL);
		return model;
	}
}

