package scau.hosp.web;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import scau.hosp.base.common.BaseController;
import scau.hosp.dictionary.GlobalCode;
import scau.hosp.entity.Binglidan;
import scau.hosp.entity.BinglidanAll;
import scau.hosp.service.IBinglidanService;

@Api(description = "病历单控制器")
@RestController
@RequestMapping("/binglidan")
public class BinglidanController extends BaseController {
	
	@Autowired
	IBinglidanService iBinglidanService;
	
	@ApiOperation("生成")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "patientId", dataType = "Integer", required = true, value = "病人id"),
		@ApiImplicitParam(paramType = "query", name = "doctorId", dataType = "Integer", required = true, value = "医生id"),
		@ApiImplicitParam(paramType = "query", name = "content", dataType = "String", required = true, value = "病历内容")
	})
	@PostMapping("/add")
	public Map<String, Object> addBinglidan(Binglidan binglidan, @RequestParam Integer patientId,
			@RequestParam Integer doctorId, @RequestParam String content) {
		Map<String, Object> model = new LinkedHashMap<>();
		binglidan.setStatus(1);
		if (iBinglidanService.add(binglidan) == 1) {
			sendCode(model, GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model, GlobalCode.OPERATION_ADDFAIL);
		return model;
	}
	
	@ApiOperation("删除(作废)")
	@PostMapping("/delete")
	public Map<String, Object> deleteBinglidan(Binglidan binglidan, @RequestParam List<Integer> idList) {
		Map<String, Object> model = new LinkedHashMap<>();
		for (Integer deleteId: idList) {
			binglidan.setId(deleteId);
			binglidan.setStatus(0);
			iBinglidanService.updateNotNull(binglidan);
		}
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("全部有效病历单")
	@ApiResponses(@ApiResponse(code = 200,message = "详情",response = Binglidan.class))
	@GetMapping("/show")
	public Map<String, Object> showBinglidan(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer pageSize) {
		Map<String, Object> model = new LinkedHashMap<>();	
		PageHelper.startPage(page, pageSize);
		List<BinglidanAll> binglidanAlls = iBinglidanService.findAllBinglidan();
		PageInfo<BinglidanAll> pageInfo = new PageInfo<BinglidanAll>(binglidanAlls);
		model.put("content", binglidanAlls);
		model.put("total", pageInfo.getTotal());
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("更新")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "id", dataType = "Integer", required = true, value = "病历id"),
		@ApiImplicitParam(paramType = "query", name = "content", dataType = "String", value = "病历内容")
	})
	@PostMapping("/update")
	public Map<String, Object> updateBinglidan(Binglidan binglidan, @RequestParam Integer id) {
		Map<String, Object> model = new LinkedHashMap<>();
		if (iBinglidanService.updateNotNull(binglidan) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPEARTION_FAIL);
		return model;
	}
}

