package scau.hosp.web;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import scau.hosp.base.common.BaseController;
import scau.hosp.dictionary.GlobalCode;
import scau.hosp.entity.Zhuyuandan;
import scau.hosp.entity.ZhuyuandanAll;
import scau.hosp.service.IZhuyuandanService;

@Api(description = "住院单控制器")
@RestController
@RequestMapping("/zhuyandan")
public class ZhuyuandanController extends BaseController {
 
	@Autowired
	IZhuyuandanService iZhuyuandanService;
	
	@ApiOperation("生成")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "patientId", dataType = "Integer", required = true, value = "病人id"),
		@ApiImplicitParam(paramType = "query", name = "doctorId", dataType = "Integer", required = true, value = "医生id"),
		@ApiImplicitParam(paramType = "query", name = "sickroomId", dataType = "Integer", required = true, value = "病床id"),
	})
	@PostMapping("/add")
	public Map<String, Object> addZhuyuandan(Zhuyuandan zhuyuandan, @RequestParam Integer patientId,
			@RequestParam Integer doctorId, @RequestParam Integer sickroomId) {
		Map<String, Object> model = new LinkedHashMap<>();
		zhuyuandan.setZhuyuanStatus(1);
		zhuyuandan.setStatus(1);
		if (iZhuyuandanService.adds(zhuyuandan) == 1) {
			sendCode(model, GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model, GlobalCode.OPERATION_ADDFAIL);
		return model;
	}
	
	@ApiOperation("删除(作废)")
	@PostMapping("/delete")
	public Map<String, Object> deleteZhuyuandan(Zhuyuandan zhuyuandan, @RequestParam List<Integer> idList) {
		Map<String, Object> model = new LinkedHashMap<>();
		for (Integer deleteId: idList) {
			zhuyuandan.setId(deleteId);
			zhuyuandan.setStatus(0);
			iZhuyuandanService.updateNotNull(zhuyuandan);
		}
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("检验单信息")
	@ApiResponses(@ApiResponse(code = 200,message = "详情",response = Zhuyuandan.class))
	@GetMapping("/show")
	public Map<String, Object> showZhuyuandan(@RequestParam(defaultValue = "1") Integer page,
			@RequestParam(defaultValue = "10") Integer pageSize){
		Map<String, Object> model = new LinkedHashMap<>();
		PageHelper.startPage(page, pageSize);
		List<ZhuyuandanAll> zhuyuandanAlls = iZhuyuandanService.findAllZhuyuandan();
		PageInfo<ZhuyuandanAll> pageInfo = new PageInfo<ZhuyuandanAll>(zhuyuandanAlls);
		model.put("content", zhuyuandanAlls);
		model.put("total", pageInfo.getTotal());
		sendCode(model,GlobalCode.OPERATION_SUCCESS);
		return model;
	}
	
	@ApiOperation("出院")
	@ApiImplicitParams({
		@ApiImplicitParam(paramType = "query", name = "id", dataType = "Date", value = "住院单id"),
	})
	@PostMapping("/out")
	public Map<String, Object> updateZhuyuandan(Zhuyuandan zhuyuandan, @RequestParam Integer id) {
		Map<String, Object> model = new LinkedHashMap<>();
		if (iZhuyuandanService.updateWithOut(zhuyuandan) == 1) {
			sendCode(model,GlobalCode.OPERATION_SUCCESS);
			return model;
		}
		sendCode(model,GlobalCode.OPEARTION_FAIL);
		return model;
	}
}