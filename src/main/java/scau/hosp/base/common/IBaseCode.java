package scau.hosp.base.common;


public interface IBaseCode {
    public int getCode();
    public String getMessage();
}
