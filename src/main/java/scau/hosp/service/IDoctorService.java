package scau.hosp.service;

import java.util.List;

import scau.hosp.base.common.IBaseService;
import scau.hosp.entity.Doctor;
import scau.hosp.entity.DoctorAll;

public interface IDoctorService extends IBaseService<Doctor>{
	
	List<DoctorAll> findAllDoctor();
	
	List<DoctorAll> findDoctor(Integer id);
}
