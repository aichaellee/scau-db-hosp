package scau.hosp.service;

import java.util.List;

import scau.hosp.base.common.IBaseService;
import scau.hosp.entity.Jianyandan;
import scau.hosp.entity.JianyandanAll;

public interface IJianyandanService extends IBaseService<Jianyandan> {

	List<JianyandanAll> findAllJianyandan();
}
