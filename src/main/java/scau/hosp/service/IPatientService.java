package scau.hosp.service;

import scau.hosp.base.common.IBaseService;
import scau.hosp.entity.Patient;

public interface IPatientService extends IBaseService<Patient> {

}
