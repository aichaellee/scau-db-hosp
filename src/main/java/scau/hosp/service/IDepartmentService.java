package scau.hosp.service;

import scau.hosp.base.common.IBaseService;
import scau.hosp.entity.Department;

public interface IDepartmentService extends IBaseService<Department> {

}
