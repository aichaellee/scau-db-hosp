package scau.hosp.service;

import java.util.List;

import scau.hosp.base.common.IBaseService;
import scau.hosp.entity.Zhuyuandan;
import scau.hosp.entity.ZhuyuandanAll;

public interface IZhuyuandanService extends IBaseService<Zhuyuandan> {

	List<ZhuyuandanAll> findAllZhuyuandan();
	
	int adds(Zhuyuandan zhuyuandan);
	
	int updateWithOut(Zhuyuandan zhuyuandan);
}
