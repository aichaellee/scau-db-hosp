package scau.hosp.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;

import scau.hosp.base.common.IBaseMapper;
import scau.hosp.base.common.IBaseService;
import scau.hosp.base.util.Reflections;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class AbstractBaseService<T> implements IBaseService<T> {
	@SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected IBaseMapper<T> baseMapper;

    private Class<?> mapperClass;

    public AbstractBaseService() {
        mapperClass = Reflections.getClassGenricType(this.getClass());

    }


    public T findByKey(Object key) {
        return baseMapper.selectByPrimaryKey(key);
    }

    public int add(T entity) {
        return baseMapper.insert(entity);
    }

    public int delete(Object key) {
        return baseMapper.deleteByPrimaryKey(key);
    }

    public int update(T entity) {
        return baseMapper.updateByPrimaryKey(entity);
    }

    public int updateNotNull(T entity) {
        return baseMapper.updateByPrimaryKeySelective(entity);
    }

    public int updateById(T entity) {
        return baseMapper.updateByPrimaryKey(entity);
    }


    public int updateSelectProperties(T entity, List<String> propertyList, List valueList) {
        Example example = new Example(mapperClass);
        //使用条件进行更新
        Example.Criteria criteria = example.createCriteria();
        if (propertyList.isEmpty() || valueList.isEmpty()) {
            throw new RuntimeException("valueList 或者 propertyList 不能为空");
        }
        if (propertyList.size() != valueList.size()) {
            throw new RuntimeException("valueList propertyList 大小必须相等");
        }
        for (int e = 0; e < propertyList.size(); ++e) {
            criteria.andEqualTo(propertyList.get(e), valueList.get(e));
        }

        return baseMapper.updateByExampleSelective(entity, example);
    }

    public int updateSelectProperties(T entity, String propertyName, Object value) {
        Example example = new Example(mapperClass);
        //使用条件进行更新
        Example.Criteria criteria = example.createCriteria();
        if (propertyName == null) {
            throw new RuntimeException("propertyName不能为空!");
        }
        criteria.andEqualTo(propertyName, value);
        return baseMapper.updateByExampleSelective(entity, example);
    }

    public List<T> find(T entity) {
        return baseMapper.select(entity);
    }

    public T findOne(T entity) {
        return baseMapper.selectOne(entity);
    }

    @Deprecated
    public List<T> findProperties(String condition, String... propertyName) throws Exception {
        Example example = new Example(mapperClass);
        Example.Criteria criteria = example.createCriteria();
        example.selectProperties(propertyName);
        criteria.andCondition(condition);
        return baseMapper.selectByExample(example);
    }


    public List<T> findSelectPropertyWhereAnd(List<String> propertyList, List valueList, String... selectProperties) {
        Example example = new Example(mapperClass);
        if (selectProperties.length >= 1) {
            example.selectProperties(selectProperties);
        }
        //不填selectProperties则select全部列
        return baseMapper.selectByExample(whereAnd(example, propertyList, valueList));
    }


    public List<T> findSelectPropertyWhereAnd(String propertyName, Object value, String... selectProperties) {
        Example example = new Example(mapperClass);
        example.selectProperties(selectProperties);
        if (selectProperties.length >= 1) {
            example.selectProperties(selectProperties);
        }
        //不填selectProperties则select全部列
        return baseMapper.selectByExample(whereAnd(example, propertyName, value));
    }
//    public List<T> findSelectPropertyWhereAnd(String[] propertyList, String[] valueList, String... selectProperty) {
//        Example example = new Example(mapperClass);
//        //使用条件查询
//        Example.Criteria criteria = example.createCriteria();
//
//        for (int e = 0; e < selectProperty.length; ++e) {
//            //写入查询属性
//            example.selectProperties(selectProperty);
//        }
//        for (int e = 0; e < propertyList.length; ++e) {
//                criteria.andEqualTo(propertyList[e],valueList[e]);
//        }
//
//        return baseMapper.selectByExample(example);
//    }

    public List<T> findSelectPropertyWhereOr(String property, String[] value, String... selectProperty) {
        Example example = new Example(mapperClass);
        //使用条件查询
        Example.Criteria criteria = example.createCriteria();
        for (int e = 0; e < selectProperty.length; ++e) {
            //写入查询属性
            example.selectProperties(selectProperty);
        }
        criteria.andEqualTo(property, value[0]);
        if (value.length >= 2) {
            for (int i = 1; i < value.length; i++) {
                example.or().andEqualTo(property, value[i]);
            }
        }
        return baseMapper.selectByExample(example);
    }


    public List<T> list(Object key) {
        return baseMapper.selectAll();
    }

    public int findCount(T entity) {
        return baseMapper.selectCount(entity);
    }

    public int findCountByCondition(String value, String... propertyName) {
        Example example = new Example(mapperClass);
        //使用条件查询
        Example.Criteria criteria = example.createCriteria();

        if (propertyName != null && propertyName.length == 1) {
            criteria.andLike(propertyName[0], "%" + value + "%");

        } else if (propertyName != null && propertyName.length > 1) {
            criteria.andLike(propertyName[0], "%" + value + "%");
            for (int i = 1; i < propertyName.length; i++) {
                Example.Criteria criteria1 = example.or();
                criteria1.andLike(propertyName[i], "%" + value + "%");
            }
        } else {
            throw new RuntimeException("propertyName不能为空!");
        }

        return baseMapper.selectCountByExample(example);
    }

    //可通过一个或多个属性值条件进行查询，参数之间要一一对应
    public List<T> findByProperties(List<String> valueList, String... propertyName) {
        Example example = new Example(mapperClass);
        //使用条件查询
        Example.Criteria criteria = example.createCriteria();
        if (propertyName != null && propertyName.length > 1) {
            if (valueList.isEmpty()) {
                throw new RuntimeException("valueList 不能为空");
            }
            for (int e = 0; e < propertyName.length; ++e) {
                criteria.andEqualTo(propertyName[e], valueList.get(e));
            }
        } else if (propertyName.length == 1) {
            for (int i = 0; i < valueList.size(); i++) {
                criteria.andEqualTo(propertyName[0], valueList.get(i));
            }
        } else {
            throw new RuntimeException("propertyName不能为空!");
        }
        return baseMapper.selectByExample(example);
    }

    public void deleteByIds(String... idArray) {
        Example example = new Example(mapperClass);
        //使用条件查询
        Example.Criteria criteria = example.createCriteria();

        Set<String> set = new HashSet<>();
        set.addAll(Arrays.asList(idArray));
        criteria.andIn("id", set);
        baseMapper.deleteByExample(example);
    }


    public List<T> listEntityByCondition(String condition, String... propertyName) {
        Example example = new Example(mapperClass);
        Example.Criteria criteria = example.createCriteria();
        if (propertyName != null && propertyName.length >= 1) {
            criteria.andLike(propertyName[0], "%" + condition + "%");
            for (int e = 1; e < propertyName.length; ++e) {
                Example.Criteria criteria1 = example.or();
                criteria1.andLike(propertyName[e], "%" + condition + "%");
            }

        } else {
            throw new RuntimeException("propertyName 不能为空");
        }
        return baseMapper.selectByExample(example);

    }

    public List<T> findAll() {
        return baseMapper.selectAll();
    }



    private Example whereAnd(Example example, List<String> propertyList, List valueList) {
        if (valueList.isEmpty()) {
            throw new RuntimeException("value 不能为空");
        }
        Example.Criteria criteria = example.createCriteria();
        for (int i = 0; i < propertyList.size(); i++) {
            criteria.andEqualTo(propertyList.get(i), valueList.get(i));
        }
        return example;
    }


    private Example whereAnd(Example example, String propertyName, Object value) {
        if (value == null) {
            throw new RuntimeException("value 不能为空");
        }
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo(propertyName, value);
        return example;
    }


}
