package scau.hosp.service.Impl;

import org.springframework.stereotype.Service;

import scau.hosp.entity.Patient;
import scau.hosp.service.IPatientService;

@Service
public class PatientImpl extends AbstractBaseService<Patient> implements IPatientService{
	
}
