package scau.hosp.service.Impl;

import org.springframework.stereotype.Service;

import scau.hosp.entity.Department;
import scau.hosp.service.IDepartmentService;

@Service
public class DepartmentImpl extends AbstractBaseService<Department> implements IDepartmentService{

}
