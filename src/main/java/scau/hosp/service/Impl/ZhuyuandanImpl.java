package scau.hosp.service.Impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scau.hosp.dao.ZhuyuandanDao;
import scau.hosp.entity.Sickroom;
import scau.hosp.entity.Zhuyuandan;
import scau.hosp.entity.ZhuyuandanAll;
import scau.hosp.service.ISickroomService;
import scau.hosp.service.IZhuyuandanService;

@Service
public class ZhuyuandanImpl extends AbstractBaseService<Zhuyuandan> implements IZhuyuandanService{

	@Autowired
	ZhuyuandanDao ZhuyuandanDao;
	
	@Autowired
	ISickroomService iSickroomService;
	
	public List<ZhuyuandanAll> findAllZhuyuandan() {
		return ZhuyuandanDao.findAllZhuyuandan();
	}

	public int adds(Zhuyuandan zhuyuandan) {
		Sickroom sickroom = new Sickroom();
		sickroom.setId(zhuyuandan.getSickroomId());
		sickroom.setBedStatus(0);
		iSickroomService.updateNotNull(sickroom);
		return add(zhuyuandan);
	}

	public int updateWithOut(Zhuyuandan zhuyuandan) {
		zhuyuandan.setZhuyuanStatus(1);
		zhuyuandan.setEndTime(new Date());
		Sickroom sickroom = iSickroomService.findByKey(findByKey(zhuyuandan).getSickroomId());
		sickroom.setBedStatus(1);
		iSickroomService.updateNotNull(sickroom);
		return updateNotNull(zhuyuandan);
	}

}
