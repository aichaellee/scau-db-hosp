package scau.hosp.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scau.hosp.dao.DoctorDao;
import scau.hosp.entity.Doctor;
import scau.hosp.entity.DoctorAll;
import scau.hosp.service.IDoctorService;

@Service
public class DoctorImpl extends AbstractBaseService<Doctor> implements IDoctorService{

	@Autowired
	DoctorDao doctorDao;
	
	public List<DoctorAll> findAllDoctor() {
		return doctorDao.findAllDoctor();
	}
	
	public List<DoctorAll> findDoctor(Integer id) {
//		return doctorDao.findDoctor(id);
		return null;
	}

}
