package scau.hosp.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import scau.hosp.dao.JianyandanDao;
import scau.hosp.entity.Jianyandan;
import scau.hosp.entity.JianyandanAll;
import scau.hosp.service.IJianyandanService;

@Service
public class JianyandanImpl extends AbstractBaseService<Jianyandan> implements IJianyandanService{

	@Autowired
	JianyandanDao JianyandanDao;
	
	public List<JianyandanAll> findAllJianyandan() {
		return JianyandanDao.findAllJianyandan();
	}

}
