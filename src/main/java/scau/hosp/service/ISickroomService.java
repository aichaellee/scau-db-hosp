package scau.hosp.service;

import java.util.List;

import scau.hosp.base.common.IBaseService;
import scau.hosp.entity.Sickroom;
import scau.hosp.entity.SickroomAll;

public interface ISickroomService extends IBaseService<Sickroom> {
	
	List<SickroomAll> findAllSickroom();
	
}
