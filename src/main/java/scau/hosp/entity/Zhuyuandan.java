package scau.hosp.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.OrderBy;

import io.swagger.annotations.ApiModelProperty;

public class Zhuyuandan implements Serializable{
	
	private static final long serialVersionUID = 100000L;
	
	@Id
    private Integer id;

    private Integer patientId;

    private Integer doctorId;

    private Integer sickroomId;

    @OrderBy("desc")
    private Date startTime;

    private Date endTime;

    @ApiModelProperty("0作废 1有效")
    private Integer status;
    
    @ApiModelProperty("0出院 1住院")
    private Integer zhuyuanStatus;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPatientId() {
        return patientId;
    }

    public void setPatientId(Integer patientId) {
        this.patientId = patientId;
    }

    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    public Integer getSickroomId() {
        return sickroomId;
    }

    public void setSickroomId(Integer sickroomId) {
        this.sickroomId = sickroomId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

	public Integer getZhuyuanStatus() {
		return zhuyuanStatus;
	}

	public void setZhuyuanStatus(Integer zhuyuanStatus) {
		this.zhuyuanStatus = zhuyuanStatus;
	}
}