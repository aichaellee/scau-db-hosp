package scau.hosp.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import scau.hosp.base.common.IBaseMapper;
import scau.hosp.entity.Jianyandan;
import scau.hosp.entity.JianyandanAll;

@Repository
public interface JianyandanDao extends IBaseMapper<Jianyandan>{
	
	List<JianyandanAll> findAllJianyandan();
}