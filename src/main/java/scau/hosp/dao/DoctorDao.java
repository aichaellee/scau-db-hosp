package scau.hosp.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import scau.hosp.base.common.IBaseMapper;
import scau.hosp.entity.Doctor;
import scau.hosp.entity.DoctorAll;

@Repository
public interface DoctorDao extends IBaseMapper<Doctor>{
	
	List<DoctorAll> findAllDoctor();
	
	Doctor findDoctorById(Integer id);
	
//	List<Doctor> findDoctor(Integer id);
}