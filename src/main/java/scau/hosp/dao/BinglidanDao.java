package scau.hosp.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import scau.hosp.base.common.IBaseMapper;
import scau.hosp.entity.Binglidan;
import scau.hosp.entity.BinglidanAll;

@Repository
public interface BinglidanDao extends IBaseMapper<Binglidan>{
	
	List<BinglidanAll> findAllBinglidan();

}