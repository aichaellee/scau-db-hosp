package scau.hosp.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import scau.hosp.base.common.IBaseMapper;
import scau.hosp.entity.Sickroom;
import scau.hosp.entity.SickroomAll;

@Repository
public interface SickroomDao extends IBaseMapper<Sickroom>{
	
	public List<SickroomAll> findAllSickroom();
	
	Sickroom findSickroonById(Integer ID);
}